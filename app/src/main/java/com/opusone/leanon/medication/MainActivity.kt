package com.opusone.leanon.medication

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.opusone.leanon.medication.adapter.TodayAlarmsAdapter
import com.opusone.leanon.medication.alarm.OoAlarmManager
import com.opusone.leanon.oocontentresolver.OoContentResolver
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oordbobserver.OoRDBObserver
import com.opusone.leanon.oordbobserver.extension.addReportMedicationObserver
import com.opusone.leanon.oordbobserver.extension.removeReportMedicationObserver
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import com.opusone.leanon.oorestmanager.model.OoMedication
import com.opusone.leanon.oorestmanager.restful.OoRestManager
import io.fabric.sdk.android.Fabric
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import com.crashlytics.android.Crashlytics
import com.opusone.leanon.medication.alarm.OoAlarmReceiver
import com.opusone.leanon.medication.util.OoPreference
import com.opusone.leanon.medication.util.OoUtil
import com.opusone.leanon.oorealmmanager.OoStateKey
import com.opusone.leanon.oorestmanager.restful.OoNetworkManager
import com.opusone.leanon.oorestmanager.restful.OoNetworkStatus
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    lateinit var medicationResults : MutableList<OoMedication>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        OoRDBObserver.removeReportMedicationObserver()
    }

    override fun onResume() {
        super.onResume()

        initWidget()
        todayMedicationList.adapter?.notifyDataSetChanged()
    }

    private fun initManagers() {
        Fabric.with(this, Crashlytics())

        OoNetworkManager.init(this)

        OoRestManager.init(true)
        OoRestManager.setReachable(OoNetworkManager) {
            applicationContext.runOnUiThread {
                longToast(R.string.toast_lost_network)
            }
        }

        OoRealmManager.initRealm(applicationContext)
        OoAlarmManager.initAlarm(applicationContext)
        OoAlarmManager.initAlarmData(user)

        if (!isValidateUser()) {
            OoAlarmManager.cancelAllAlarms(applicationContext, OoAlarmReceiver.ACTION_ALARM)
            clearRealm()
        }
        user?.id?.let {
            syncDBwithServer(it)
        }

        addNetwrokChangedObserver()
    }

    private fun addNetwrokChangedObserver() {
        OoNetworkManager.onNetworkChangeListener = { status ->
            when (status) {
                OoNetworkStatus.LOST -> {
//                    runOnUiThread {
//                        longToast(R.string.toast_lost_network)
//                    }
                }

                OoNetworkStatus.CELLULAR -> {
//                    runOnUiThread {
//                        longToast(R.string.toast_available_mobile_network)
//                    }
                }

                OoNetworkStatus.WiFi -> {
//                    runOnUiThread {
//                        longToast(R.string.toast_available_wifi_network)
//                    }
                }
            }
        }
    }

    fun clearRealm() {
        OoAlarmManager.medicationRealmInfo?.first?.close()
        OoAlarmManager.medicationRealmInfo = null
        OoRealmManager.clear()
    }

    private fun isValidateUser(): Boolean {
        var ret = true
        try {
            user = OoContentResolver.getUserInfo(contentResolver)

            if (user == null || user?.id == null || (user?.id ?: "").isEmpty()) {
                ret = false
            }

            val preferenceUserId = OoPreference.getUserID(this) ?: ""
            if (!preferenceUserId.isEmpty() && (user?.id != preferenceUserId)) {
                ret = false
            }

            OoPreference.setUserID(this, user?.id ?: "")

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            ret = false
        }
        return ret
    }

    private fun initWidget() {
        goEditPage.isEnabled = false

        dataExist.visibility = View.GONE

        noData.visibility = View.VISIBLE

        addMedicine.setOnClickListener {
            if (!OoNetworkManager.isReachable()) {
                longToast(R.string.toast_lost_network)
                return@setOnClickListener
            }
            startActivity(Intent(applicationContext, SetAlarmActivity::class.java))
        }

        OoRealmManager.getState(OoStateKey.medicationState)?.let {state ->
            setAlarmStateUI(state)
        }

        alarmToggleBtn.setOnCheckedChangeListener { compoundButton, state ->
            setAlarmStateUI(state)
        }

        user?.id?.let {id ->
            OoRDBObserver.addReportMedicationObserver(id) {
                medicationResults = it.toMutableList()
                Log.i(TAG, "medicationResults : $medicationResults")
                setMainPage()
            }
        }

        goEditPage.setOnClickListener {
            if (!OoNetworkManager.isReachable()) {
                longToast(R.string.toast_lost_network)
                return@setOnClickListener
            }
            startActivity(Intent(applicationContext, EditAllAlarmActivity::class.java))
        }
    }

    private fun setAlarmStateUI(state : Boolean) {
        alarmToggleBtn.isChecked = state
        user?.id?.let {id ->
            if (state) {
                OoRealmManager.updateState(id, OoStateKey.medicationState, true)
                alarmStateText.text = "복약 알람 켜짐"
                alarmStateText.setTextColor(getColor(R.color.colorGreen))
            } else {
                OoRealmManager.updateState(id, OoStateKey.medicationState, false)
                alarmStateText.text = "복약 알람 꺼짐"
                alarmStateText.setTextColor(getColor(R.color.colorGray))

            }
        }
    }

    private fun syncDBwithServer(id:String) {
        if (OoRealmManager.getMedicineCount() == 0) {
            OoRestManager.getMedications(id) {error, response ->
                response?.medications?.let { list ->
                    if (list.isNotEmpty()) {
                        for (i in list) {
                            val ooRmMedicine = OoRmMedicine()
                            ooRmMedicine.medicineId = i.id
                            ooRmMedicine.medicineName = "${i.name}"
                            ooRmMedicine.alarmId = i.alarmId?.toInt() ?: 0
                            ooRmMedicine.hour = i.hour?.toInt() ?: -1
                            ooRmMedicine.min = i.min?.toInt() ?: -1
                            ooRmMedicine.photoUri = i.picture
                            val list = RealmList<Boolean>()
                            list.addAll(i.weekdaysInfo ?: listOf(false, false, false, false, false, false, false, false))
                            ooRmMedicine.weekdaysInfo = list
                            OoRealmManager.create(ooRmMedicine)
                            OoAlarmManager.registAlarm(applicationContext, ooRmMedicine.alarmId, OoAlarmReceiver.ACTION_ALARM, ooRmMedicine)
                        }
                    }
                } ?: runOnUiThread {
                    longToast(R.string.response_error)
                }
            }
        }
    }

    private fun setMainPage() {
        val todayList = mutableListOf<OoMedication>()

        for (i in medicationResults) {
            val everyday = i.weekdaysInfo?.get(0) ?: false
            val today = i.weekdaysInfo?.get(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) ?: false
            if(everyday || today) {
                todayList.add(i)
            }
        }
        if (medicationResults.count() == 0) {
            goEditPage.isEnabled = false
            dataExist.visibility = View.GONE
            noData.visibility = View.VISIBLE
            return
        } else {
            noData.visibility = View.GONE
            dataExist.visibility = View.VISIBLE
            goEditPage.isEnabled = true
            val dateFormat = SimpleDateFormat("yyyy년 M월 d일", Locale.getDefault())
            dateOfToday.text = dateFormat.format(Calendar.getInstance().timeInMillis)
            if(todayList.count() == 0) {
                noTodayDataText.visibility = View.VISIBLE
                todayMedicationList.visibility = View.GONE
            } else {
                noTodayDataText.visibility = View.GONE
                todayMedicationList.visibility = View.VISIBLE
                todayMedicationList.adapter = TodayAlarmsAdapter(todayList)
                todayMedicationList.adapter?.notifyDataSetChanged()
                todayMedicationList.addItemDecoration(OoUtil.getDivider(applicationContext))
                todayList.sortBy {
                    val id = it.alarmId ?: "0"
                    id.toInt()
                }
            }
            return
        }
    }
    companion object {
        var user : User? = null
    }
}
