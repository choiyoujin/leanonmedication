package com.opusone.leanon.medication

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.opusone.leanon.medication.alarm.OoAlarmManager
import com.opusone.leanon.medication.alarm.OoAlarmReceiver
import com.opusone.leanon.medication.dialog.ErrorDialog
import com.opusone.leanon.medication.dialog.GetPictureDialog
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.OoStateKey
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import com.opusone.leanon.oorestmanager.params.OoParamRegisterMedication
import com.opusone.leanon.oorestmanager.params.OoParamUpdateMedication
import com.opusone.leanon.oorestmanager.restful.OoNetworkManager
import com.opusone.leanon.oorestmanager.restful.OoRestManager
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_set_alarm.*
import org.jetbrains.anko.longToast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class SetAlarmActivity : AppCompatActivity() {
    private val TAG = "SetAlarmActivity"

    private var weekdays = mutableListOf(true, false, false, false, false, false, false, false)
    private var getPictureDialog : GetPictureDialog? = null
    private val REQUEST_TAKE_PHOTO = 100
    private val REQUEST_TAKE_GALLERY = 200
    private var currentPhotoPath : String = ""
    private var photoUri : Uri?= null
    private var photoFile: File? = null
    private var medicineId : String?= null
    private var havetoCheck : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_alarm)

        initWidget()
    }

    private fun initWidget() {
        intent.getStringExtra("medicineId")?.let {
            showOriginInfo(it)
        }
        addMedicinePicture.setOnClickListener {
            getMedicineImage()
        }
        changeMedicinePicture.setOnClickListener {
            getMedicineImage()
        }
        setRepeatDaysBtn()
        set_cancel.setOnClickListener {
            finish()
        }
        set_done.setOnClickListener {
            if (!OoNetworkManager.isReachable()) {
                longToast(R.string.toast_lost_network)
                return@setOnClickListener
            }
            setMedicineAlarm()
        }
    }

    private fun showOriginInfo(id: String?) {
        id?.let {
            OoRealmManager.findMedicineById(id) {
                it?.let {
                    medicineId = id
                    havetoCheck = it.alarmId

                    setAlarmTitle.text = it.medicineName

                    Glide.with(this)
                        .load(it.photoUri)
                        .circleCrop()
                        .placeholder(R.drawable.default_image)
                        .into(addMedicinePicture)
                    changeMedicinePicture.visibility = View.VISIBLE

                    medicineNameEdit.setText(it.medicineName)

                    timePicker.hour = it.hour
                    timePicker.minute = it.min

                    weekdays = it.weekdaysInfo
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_TAKE_PHOTO && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            takePictureIntent()
            getPictureDialog?.dismiss()
        }
        if (requestCode == REQUEST_TAKE_GALLERY && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            takeGalleryIntent()
            getPictureDialog?.dismiss()
        }
    }
    private fun takePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (takePictureIntent.resolveActivity(packageManager) != null) {
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
            }

            photoFile?.let{
                val photoURI = FileProvider.getUriForFile(
                    this,
                    "com.opusone.leanon.medication.fileprovider",
                    it
                )
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            photoFile?.let {
                it.delete()
                photoFile = null
            }
            return
        }
        when(requestCode) {
            REQUEST_TAKE_PHOTO -> {
                val file = File(currentPhotoPath)
                Log.e(TAG, currentPhotoPath)
                photoUri = Uri.fromFile(file)
                setImage()
            }
            REQUEST_TAKE_GALLERY -> {
                var albumFile: File? = null
                try {
                    albumFile = createImageFile()
                } catch (ex: IOException) {
                }
                if (albumFile != null) {
                    photoUri = data?.data
                    setImage()
                }
            }
        }
    }

    private fun setImage() {
        photoUri?.let {
            try {
                Glide.with(this)
                    .load(it)
                    .placeholder(R.drawable.default_image)
                    .circleCrop()
                    .into(addMedicinePicture)
                changeMedicinePicture.visibility = View.VISIBLE
            } catch (e : IOException) {
                e.printStackTrace()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        currentPhotoPath = image.absolutePath
        return image
    }

    private fun takeGalleryIntent() {
        val intent = Intent()
        intent.action = Intent.ACTION_PICK
        intent.type = MediaStore.Images.Media.CONTENT_TYPE

        startActivityForResult(intent, REQUEST_TAKE_GALLERY)
    }

    private fun getMedicineImage() {
        if (getPictureDialog == null) {
            getPictureDialog = GetPictureDialog(this)
        }
        getPictureDialog?.onCameraIntent = {
            if (checkSelfPermission(Manifest.permission.CAMERA).equals(PackageManager.PERMISSION_GRANTED)
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).equals(PackageManager.PERMISSION_GRANTED)) {
                takePictureIntent()
                getPictureDialog?.dismiss()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_TAKE_PHOTO
                )
            }
        }
        getPictureDialog?.onGalleryIntent = {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).equals(PackageManager.PERMISSION_GRANTED)) {
                takeGalleryIntent()
                getPictureDialog?.dismiss()
            } else {

                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_TAKE_GALLERY
                )
            }
        }
        getPictureDialog?.show()
    }

    private fun setMedicineAlarm() {
        var alarmInfo : OoRmMedicine? = null
        if (medicineNameEdit.text.toString().isNotEmpty()) {
            when(medicineId) {
                null -> {
                    alarmInfo = setMedicine()
                    MainActivity.user?.id?.let {
                        OoRealmManager.updateState(it, OoStateKey.medicationState, true)
                    }
                }
                else -> {
                    medicineId?.let {
                        alarmInfo = editMedicine(it)
                    }
                }
            }
            alarmInfo?.let {
                OoAlarmManager.registAlarm(applicationContext, getAlarmSetting().alarmId, OoAlarmReceiver.ACTION_ALARM, it)
                startActivity(Intent(applicationContext, EditAllAlarmActivity::class.java))
                finish()
            }
        }else {
            getErrorDialog(this)
        }
    }

    private fun getAlarmSetting() : OoRmMedicine{
        val alarmInfo = OoRmMedicine()
        alarmInfo.medicineName = medicineNameEdit.text.toString()
        var list = RealmList<Boolean>()
        var everyday = true
        for (i in 1..7) {
            if (!weekdays[i]) {
                everyday = false
            }
        }
        if (everyday) {
            list = RealmList(true, false, false, false, false, false, false, false)
        } else {
            list.addAll(weekdays)
        }
        alarmInfo.weekdaysInfo = list
        alarmInfo.hour = timePicker.hour
        alarmInfo.min = timePicker.minute
        photoUri?.let {
            alarmInfo.photoUri = it.toString()
        }
        if (timePicker.minute < 10) {
            alarmInfo.alarmId = "${timePicker.hour}0${timePicker.minute}".toInt()
        } else {
            alarmInfo.alarmId = "${timePicker.hour}${timePicker.minute}".toInt()
        }
        Log.i(TAG, "alarm id is ${alarmInfo.alarmId}")
        return alarmInfo
    }

    private fun setMedicine() : OoRmMedicine? {
        var isError = false
        val alarmInfo = getAlarmSetting()
        val createParam = OoParamRegisterMedication()
        createParam.userToken = MainActivity.user?.userToken
        createParam.seniorId = MainActivity.user?.id
        createParam.alarmId = alarmInfo.alarmId.toString()
        createParam.name = alarmInfo.medicineName
        createParam.hour = alarmInfo.hour.toString()
        createParam.min = alarmInfo.min.toString()
        createParam.picture = alarmInfo.photoUri
        createParam.weekdaysInfo = alarmInfo.weekdaysInfo
        createParam.alarmId = alarmInfo.alarmId.toString()
        OoRestManager.registerMedication(createParam) { error, response ->
            Log.i(TAG, "error is $error, response is $response")
            response?.let {
                alarmInfo.medicineId = it.medication?.id
                OoRealmManager.create(alarmInfo)
            } ?: runOnUiThread {
                longToast(R.string.response_error)
                isError = true
            }
        }
        return if (isError) null else alarmInfo
    }

    private fun editMedicine(id: String) : OoRmMedicine?{
        var isError = false
        val alarmInfo = getAlarmSetting()
        alarmInfo.medicineId = id
        val updateParam = OoParamUpdateMedication()
        updateParam.medicationId = id
        updateParam.userToken = MainActivity.user?.userToken
        updateParam.seniorId = MainActivity.user?.id
        updateParam.alarmId = alarmInfo.alarmId.toString()
        updateParam.name = alarmInfo.medicineName
        updateParam.hour = alarmInfo.hour.toString()
        updateParam.min = alarmInfo.min.toString()
        updateParam.picture = alarmInfo.photoUri
        updateParam.weekdaysInfo = weekdays
        updateParam.alarmId = alarmInfo.alarmId.toString()

        OoRestManager.updateMedication(updateParam) {error, response ->
            Log.i(TAG, "error is $error, response is $response")
            response?.let {
                OoRealmManager.updateMedicineById(id) { medi ->
                    medi.medicineName = alarmInfo.medicineName
                    medi.weekdaysInfo = alarmInfo.weekdaysInfo
                    medi.hour = alarmInfo.hour
                    medi.min = alarmInfo.min
                    medi.alarmId = alarmInfo.alarmId
                    photoUri?.let {
                        medi.photoUri = it.toString()
                    }
                }
                havetoCheck?.let {
                    if (OoRealmManager.getSimultaneousAlarmsCount(it) == 0) {
                        OoAlarmManager.cancelAlarm(this, it, OoAlarmReceiver.ACTION_ALARM)
                    }
                }
            } ?:
            runOnUiThread {
                longToast(R.string.response_error)
                isError = true
            }
        }
        return if(isError) null else alarmInfo
    }

    private fun setRepeatDaysBtn() {
        everyday.isSelected = weekdays[0]
        mon.isSelected = weekdays[2]
        tue.isSelected = weekdays[3]
        wed.isSelected = weekdays[4]
        thu.isSelected = weekdays[5]
        fri.isSelected = weekdays[6]
        sat.isSelected = weekdays[7]
        sun.isSelected = weekdays[1]
        everyday.setOnClickListener {
            setButtonState(it, 0)
            if (weekdays[0]) {
                mon.isSelected = false
                tue.isSelected = false
                wed.isSelected = false
                thu.isSelected = false
                fri.isSelected = false
                sat.isSelected = false
                sun.isSelected = false
                for (i in 1..7) {
                    weekdays[i] = false
                }
            }
        }
        mon.setOnClickListener {
            setButtonState(it, 2)
            relativeButtonState()
        }
        tue.setOnClickListener {
            setButtonState(it, 3)
            relativeButtonState()
        }
        wed.setOnClickListener {
            setButtonState(it, 4)
            relativeButtonState()
        }
        thu.setOnClickListener {
            setButtonState(it, 5)
            relativeButtonState()
        }
        fri.setOnClickListener {
            setButtonState(it, 6)
            relativeButtonState()
        }
        sat.setOnClickListener {
            setButtonState(it, 7)
            relativeButtonState()
        }
        sun.setOnClickListener {
            setButtonState(it, 1)
            relativeButtonState()
        }
    }
    private fun onlyOneSelected() : Boolean {
        var count = 0
        for (i in weekdays) {
            if(i) {
                count++
            }
        }
        return count == 1
    }

    private fun setButtonState(view: View, pos : Int) {
        if (!onlyOneSelected()|| !view.isSelected) {
            view.isSelected = !weekdays[pos]
            weekdays[pos] = !weekdays[pos]
        }
    }

    private fun relativeButtonState() {
        if (weekdays[0]) {
            weekdays[0] = false
            everyday.isSelected = false
        }
    }

    private var errorDialog: ErrorDialog? = null
    private fun getErrorDialog(context: Context) {
        if (errorDialog == null) {
            errorDialog = ErrorDialog(context)
        }
        errorDialog?.show()
    }
}
