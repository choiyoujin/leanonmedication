package com.opusone.leanon.medication.util

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.opusone.leanon.medication.R
import com.opusone.leanon.oorestmanager.model.OoMedication
import java.text.SimpleDateFormat
import java.util.*

object OoUtil {
    fun getTimeString(info : OoMedication) : String {
        var ringTime = ""
        var minute = ""
        info.min?.toInt()?.let {
            if(it < 10) {
                minute = "0${info.min}"
            } else {
                minute = "${info.min}"
            }
        }
        info.hour?.toInt()?.let {
            if (it < 12) {
                ringTime = "오전 ${it}:${minute}"
            } else if (it == 12) {
                ringTime = "오후 ${it}:${minute}"
            } else {
                ringTime = "오후 ${it-12}:${minute}"
            }
        }

        return ringTime
    }

    fun isDateDiff(a : Long?, b : Long?) : Boolean {
        val dateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
        return dateFormat.format(a).toInt() > dateFormat.format(b).toInt()
    }

    fun getDivider(context: Context) : DividerItemDecoration{
        val divider = object : DividerItemDecoration(context, LinearLayoutManager.VERTICAL) {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view)
                parent.adapter?.itemCount?.let {
                    if (position == it-1) {
                        outRect.setEmpty()
                    } else {
                        super.getItemOffsets(outRect, view, parent, state)
                    }
                }
            }
        }
        context.getDrawable(R.drawable.stroke_underline_black)?.let {
            divider.setDrawable(it)
        }
        return divider
    }
}