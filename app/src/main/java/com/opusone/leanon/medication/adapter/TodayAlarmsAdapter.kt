package com.opusone.leanon.medication.adapter

import android.content.Context
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.database.snapshot.BooleanNode
import com.opusone.leanon.medication.R
import com.opusone.leanon.medication.util.OoUtil
import com.opusone.leanon.medication.util.OoUtil.isDateDiff
import com.opusone.leanon.oorestmanager.model.OoMedication
import kotlinx.android.synthetic.main.item_medicine_today.view.*
import java.text.SimpleDateFormat
import java.util.*

class TodayAlarmsAdapter(var todayAlarmList: List<OoMedication>) : RecyclerView.Adapter<TodayAlarmsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_medicine_today, parent, false))
    }

    override fun getItemCount(): Int {
        return todayAlarmList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val info = todayAlarmList[position]
        holder.time.text = OoUtil.getTimeString(info)
        info.picture?.let {
            Glide.with(holder.itemView.context)
                .load(it)
                .circleCrop()
                .placeholder(R.drawable.profile_image)
                .into(holder.image)
        }
        holder.name.text = info.name

        if (isDateDiff(Calendar.getInstance().timeInMillis, info.timestamp)) {
            setTaken(holder, false)
        } else {
            info.taken?.toBoolean()?.let {
                setTaken(holder, it)
            }
        }
    }

    fun setTaken(holder: ViewHolder, boolean : Boolean) {
        if(boolean) {
            holder.isTakenText.text = "복약 완료"
            holder.isTakenText.setTextColor(holder.itemView.context.getColor(R.color.colorBlue))
            holder.isTakenImage.isEnabled = true
        } else {
            holder.isTakenText.text = "복약 예정"
            holder.isTakenText.setTextColor(holder.itemView.context.getColor(R.color.colorGray))
            holder.isTakenImage.isEnabled = false
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time = itemView.todayMedicineTIme
        val image = itemView.todayMedicineImage
        val name = itemView.todayMedicineName
        val isTakenText = itemView.isTakenText
        val isTakenImage = itemView.isTakenImage
    }
}