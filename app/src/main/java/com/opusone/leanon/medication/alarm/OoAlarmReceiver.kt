package com.opusone.leanon.medication.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.OoStateKey
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import java.util.*

class OoAlarmReceiver : BroadcastReceiver() {
    private val TAG = "OoAlarmReceiver"

    private val MEDICINE = "opusone.leanon.medication"
    private var alarmInfo = OoRmMedicine()
    private val list = mutableListOf<OoRmMedicine>()

    override fun onReceive(context: Context, intent: Intent) {
        init(context, intent)
        when(intent.action) {
            ACTION_ALARM -> {
                alarmProcess(context)
            }
            ACTION_REALARM -> {
                reAlarmProcess(context)
            }
            Intent.ACTION_BOOT_COMPLETED -> {
                OoAlarmManager.registAlarmsIfBooted(context, ACTION_ALARM)
            }
        }
    }

    private fun init(context: Context, intent: Intent) {
        OoRealmManager.initRealm(context)
        OoAlarmManager.initAlarm(context)
        alarmInfo.alarmId = intent.getIntExtra(KEY_ALARM_ID, 0)
        OoRealmManager.findSimultaneousAlarmsList(alarmInfo.alarmId) {
            it?.let {
                if (it.isNotEmpty()) {
                    alarmInfo.hour = it.first().hour
                    alarmInfo.min = it.first().min
                    alarmInfo.photoUri = it.first().photoUri
                    for (i in it) {
                        val everyday = i.weekdaysInfo.get(0) ?: false
                        val today = i.weekdaysInfo.get(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) ?: false
                        if(everyday || today) {
                            list.add(i)
                        }
                    }
                }
            }
        }
    }

    private fun sendLauncherBroadcast(context: Context) {
        val nameList  = arrayListOf<String?>()
        for (i in list) {
            nameList.add(i.medicineName)
        }
        val idList  = arrayListOf<String?>()
        for (i in list) {
            idList.add(i.medicineId)
        }
        Log.i(TAG, "idList is $idList, nameList is $nameList")
        val sendIntent = Intent(MEDICINE)
        sendIntent.putExtra(KEY_ALARM_ID, idList)
        sendIntent.putExtra(KEY_ALARM_HOUR, alarmInfo.hour)
        sendIntent.putExtra(KEY_ALARM_MIN, alarmInfo.min)
        sendIntent.putExtra(KEY_ALARM_NAME, nameList)
        sendIntent.putExtra(KEY_ALARM_PHOTO, alarmInfo.photoUri)
        context.sendBroadcast(sendIntent)
    }

    private fun alarmProcess(context: Context) {
        Log.e(TAG, "alarm ${list.count()}")
        OoRealmManager.getState(OoStateKey.medicationState)?.let {
            if (it&& list.count() > 0) {
                Log.e(TAG, "alarm ring ring")
                sendLauncherBroadcast(context)
            }
        }
        OoAlarmManager.registAlarm(context, alarmInfo.alarmId, ACTION_ALARM, alarmInfo)
    }

    private fun reAlarmProcess(context: Context) {
        OoRealmManager.getState(OoStateKey.medicationState)?.let{
            if (it) {
                sendLauncherBroadcast(context)
            }
        }
    }

    companion object {
        val ACTION_REALARM = "com.opusone.leanon.medication.ReAlarm"
        val ACTION_ALARM = "com.opusone.leanon.medication.Alarm"
        val KEY_ALARM_ID = "com.opusone.leanon.medication.AlarmId"
        val KEY_ALARM_HOUR = "com.opusone.leanon.medication.AlarmHour"
        val KEY_ALARM_MIN = "com.opusone.leanon.medication.AlarmMin"
        val KEY_ALARM_NAME = "com.opusone.leanon.medication.AlarmName"
        val KEY_ALARM_WEEK = "com.opusone.leanon.medication.AlarmWeek"
        val KEY_ALARM_PHOTO = "com.opusone.leanon.medication.AlarmPhoto"
    }
}
