package com.opusone.leanon.medication

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.opusone.leanon.medication.adapter.AllAlarmsAdapter
import com.opusone.leanon.medication.alarm.OoAlarmManager
import com.opusone.leanon.medication.dialog.DeleteDialog
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import io.realm.*
import kotlinx.android.synthetic.main.activity_edit_all_alarm.*
import kotlinx.android.synthetic.main.activity_main.*
import io.realm.OrderedCollectionChangeSet
import io.realm.RealmResults
import io.realm.OrderedRealmCollectionChangeListener
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.opusone.leanon.medication.util.OoUtil
import com.opusone.leanon.oordbobserver.OoRDBObserver
import com.opusone.leanon.oordbobserver.extension.addReportMedicationObserver
import com.opusone.leanon.oorestmanager.model.OoMedication
import com.opusone.leanon.oorestmanager.restful.OoNetworkManager
import org.jetbrains.anko.longToast


class EditAllAlarmActivity : AppCompatActivity() {
    private val TAG = "EditAllAlarmActivity"

    private lateinit var allAlarmsAdapter : AllAlarmsAdapter
    var deleteDialog: DeleteDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_all_alarm)
    }

    override fun onResume() {
        super.onResume()

        initWidget()
    }

    private fun initWidget() {
        edit_done.setOnClickListener {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }

        addMedicine_editPage.setOnClickListener {
            if (!OoNetworkManager.isReachable()) {
                longToast(R.string.toast_lost_network)
                return@setOnClickListener
            }
            startActivity(Intent(applicationContext, SetAlarmActivity::class.java))
        }
        allMedicationList.addItemDecoration(OoUtil.getDivider(applicationContext))
        MainActivity.user?.id?.let {
            OoRDBObserver.addReportMedicationObserver(it) {
                val sorted = it.sortedBy {
                    val id = it.alarmId ?: "0"
                    id.toInt()
                }

                allAlarmsAdapter = AllAlarmsAdapter(this, sorted)
                allAlarmsAdapter.onItemEditClick = {
                    if (!OoNetworkManager.isReachable()) {
                        longToast(R.string.toast_lost_network)

                    } else {
                        it?.let {id ->
                            val intent = Intent(this, SetAlarmActivity::class.java)
                            intent.putExtra("medicineId", id)
                            startActivity(intent)
                        }
                    }
                }

                allAlarmsAdapter.onItemDeleteClick = {id, info ->
                    info.id?.let {
                        if (!OoNetworkManager.isReachable()) {
                            longToast(R.string.toast_lost_network)

                        } else {
                            OoRealmManager.findMedicineById(it) {
                                it?.let {
                                    val item = it
                                    getDeleteDialog(this, id, item)
                                }
                            }
                        }
                    }
                }

                OoAlarmManager.medicationRealmInfo?.second?.addChangeListener{ results, changeSet ->
                    allAlarmsAdapter.notifyDataSetChanged()
                }
                allMedicationList.adapter = allAlarmsAdapter
            }
        }
    }

    private fun getDeleteDialog(context: Context, alarmId: Int, alarmInfo: OoRmMedicine) {
        if (deleteDialog == null) {
            deleteDialog = DeleteDialog(context, alarmId, alarmInfo)
            deleteDialog?.onDeleteDialog = onDeleteDialog@{
                if (!OoNetworkManager.isReachable()) {
                    longToast(R.string.toast_lost_network)
                    return@onDeleteDialog
                }
                OoAlarmManager.deleteAlarm(context, alarmId, alarmInfo)
                allAlarmsAdapter.notifyDataSetChanged()
            }
        }
        deleteDialog?.show()
        deleteDialog = null
    }
}
