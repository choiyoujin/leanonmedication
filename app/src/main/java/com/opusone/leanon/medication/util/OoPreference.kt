package com.opusone.leanon.medication.util

import android.content.Context
import android.content.SharedPreferences

object OoPreference {
    private const val packageName = "com.opusone.leanon.medication"

    private const val KEY_USER_ID = "$packageName.userId"

    private fun pref(context: Context): SharedPreferences? {
        return context.getSharedPreferences(packageName, Context.MODE_PRIVATE)
    }

    private fun putString(pref: SharedPreferences, key: String, value: String?) {
        val editor = pref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun getString(pref: SharedPreferences, key: String, defValue: String?): String? {
        return pref.getString(key, defValue)
    }

    private fun putBoolean(pref: SharedPreferences, key: String, value: Boolean) {
        val editor = pref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    private fun getBoolean(pref: SharedPreferences, key: String): Boolean {
        return pref.getBoolean(key, false)
    }

    private fun putInt(pref: SharedPreferences, key: String, value: Int) {
        val editor = pref.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    private fun getInt(pref: SharedPreferences, key: String, default: Int): Int {
        return pref.getInt(key, default)
    }

    private fun putLong(pref: SharedPreferences, key: String, value: Long) {
        val editor = pref.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    private fun getLong(pref: SharedPreferences, key: String, default: Long): Long {
        return pref.getLong(key, default)
    }

    fun setUserID(context: Context, userID: String?) {
        pref(context)?.let {
            putString(
                it,
                KEY_USER_ID,
                userID
            )
        }
    }

    fun getUserID(context: Context): String? {
        var ret: String? = null
        pref(context)?.let {
            ret = getString(
                it,
                KEY_USER_ID,
                null
            )
        }
        return ret
    }


    fun clear(context: Context) {
        pref(context)?.let {
            val editor = it.edit()
            editor.clear()
            editor.commit()
        }
    }
}