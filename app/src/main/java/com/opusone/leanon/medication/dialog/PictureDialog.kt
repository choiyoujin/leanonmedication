package com.opusone.leanon.medication.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.opusone.leanon.medication.R
import kotlinx.android.synthetic.main.dialog_getpicture.*

class GetPictureDialog(context: Context) : Dialog(context) {
    var onCameraIntent : OnCameraIntent? = null
    var onGalleryIntent : OnGalleryIntent? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_getpicture)

        initWidget()
    }

    private fun initWidget() {
        take_photo.setOnClickListener {
            onCameraIntent?.let {
            it()
        } }
        get_photo_from_gallery.setOnClickListener {
            onGalleryIntent?.let {
                it()
            }
        }
        cancel_photo.setOnClickListener { dismiss() }
    }
}
typealias OnCameraIntent = () -> Unit
typealias OnGalleryIntent = () -> Unit
