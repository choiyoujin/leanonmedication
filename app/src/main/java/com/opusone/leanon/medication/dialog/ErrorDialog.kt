package com.opusone.leanon.medication.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.opusone.leanon.medication.R
import kotlinx.android.synthetic.main.dialog_error.*

class ErrorDialog (context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_error)
        close_dialog.setOnClickListener{
            dismiss()
        }
    }
}