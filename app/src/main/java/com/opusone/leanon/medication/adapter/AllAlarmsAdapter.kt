package com.opusone.leanon.medication.adapter

import android.content.Context
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.opusone.leanon.medication.R
import com.opusone.leanon.medication.alarm.OoAlarmManager
import com.opusone.leanon.medication.util.OoUtil
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import com.opusone.leanon.oorestmanager.model.OoMedication
import io.realm.OrderedRealmCollection
import io.realm.RealmList
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_medicine_all.view.*

class AllAlarmsAdapter(var context: Context, var allAlarmList: List<OoMedication>) : RecyclerView.Adapter<AllAlarmsAdapter.viewHolder>() {
    private val TAG = "AllAlarmsAdapter"

    var onItemEditClick : OnItemEditClick? = null
    var onItemDeleteClick : OnItemDeleteClick? = null

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        val v = holder
        val info = allAlarmList[position]
        v.days.text = getRepeatingDay(info.weekdaysInfo)
        v.time.text = OoUtil.getTimeString(info)
        info.picture?.let {
            Glide.with(holder.itemView.context)
                .load(it)
                .placeholder(R.drawable.profile_image)
                .circleCrop()
                .into(v.picture)
        }
        v.name.text = info.name
        v.edit.setOnClickListener {
            onItemEditClick?.let {
                it(info.id)
            }
        }
        v.delete.setOnClickListener {
            onItemDeleteClick?.let {
                info.alarmId?.toInt()?.let {
                    it(it, info)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return allAlarmList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder{
        return viewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_medicine_all, parent, false))
    }

    private fun getRepeatingDay(list: MutableList<Boolean>?): String {
        var rpDayKorean = ""
        list?.let {
            if (list[0]) {
                return "매일"
            }
            if (list[2]) {
                rpDayKorean += "월"
            }
            if (list[3]) {
                rpDayKorean += "화"
            }
            if (list[4]) {
                rpDayKorean += "수"
            }
            if (list[5]) {
                rpDayKorean += "목"
            }
            if (list[6]) {
                rpDayKorean += "금"
            }
            if (list[7]) {
                rpDayKorean += "토"
            }
            if (list[1]) {
                rpDayKorean += "일"
            }
        }
        return rpDayKorean
    }

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val days = itemView.medicineRepeatDays
        val time = itemView.allMedicineTIme
        val picture = itemView.allMedicineImage
        val name = itemView.allMedicineName
        val edit = itemView.editAlarm
        val delete = itemView.deleteAlarm
    }
}
typealias OnItemEditClick = (String?) -> Unit
typealias OnItemDeleteClick = (Int, OoMedication) -> Unit