package com.opusone.leanon.medication.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.opusone.leanon.medication.MainActivity
import com.opusone.leanon.medication.R
import com.opusone.leanon.oocontentresolver.model.User
import com.opusone.leanon.oorealmmanager.OoRealmManager
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import com.opusone.leanon.oorealmmanager.model.OoRmMessage
import com.opusone.leanon.oorestmanager.restful.OoRestManager
import io.realm.Realm
import io.realm.RealmObject
import java.util.*
import io.realm.RealmObject.deleteFromRealm
import io.realm.RealmResults
import org.jetbrains.anko.longToast
import org.jetbrains.anko.runOnUiThread


object OoAlarmManager {
    private val TAG = "OoAlarmManager"
    private var am: AlarmManager? = null
    var medicationRealmInfo: Pair<Realm, RealmResults<OoRmMedicine>>? = null

    fun initAlarm(ctx: Context) {
        if (am == null) {
            am = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            Log.d(TAG, "alarmInit")
        }
    }

    fun initAlarmData(user: User?) {
        user?.id?.let {
            medicationRealmInfo = OoRealmManager.getMedicineList()
        }
    }

    fun registAlarm(ctx: Context, alarmID: Int, action: String, alarmInfo: OoRmMedicine) {
        am?.let {
            if (isExistAlarm(ctx, alarmID, action)) {
                Log.d(TAG, "registAlarm : alarmExisting")
                cancelAlarm(ctx, alarmID, action)
            }
            val i = createPendingIntent(ctx, alarmID, action)
            it.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getTriggerAtMillis(alarmInfo.hour, alarmInfo.min),
                i)
            Log.d(TAG, "registAlarm 성공 : ${alarmInfo.alarmId}, ${alarmInfo.hour}시 ${alarmInfo.min}분")
        }
    }

    fun registAlarmAgain(ctx: Context, alarmID: Int, action: String) {
        am?.let {
            if (isExistAlarm(ctx, -alarmID, action)) {
                Log.d(TAG, "registAlarmAgain : alarmExisting")
                cancelAlarm(ctx, -alarmID, action)
            }
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            val tenMinuteLater = calendar.timeInMillis + 600000

            val i = createPendingIntent_Re(ctx, alarmID, action)
            it.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, tenMinuteLater, i)
            Log.d(TAG, "registAlarm 10min later")
        }
    }

    private fun getTriggerAtMillis(hourOfDay: Int, minute: Int): Long {
        val currentCalendar = Calendar.getInstance()
        val currentHourOfDay = currentCalendar.get(Calendar.HOUR_OF_DAY)
        val currentMinute = currentCalendar.get(Calendar.MINUTE)

        return if (currentHourOfDay < hourOfDay || currentHourOfDay == hourOfDay && currentMinute < minute) {
            getTimeInMillis(false, hourOfDay, minute)
        } else {
            getTimeInMillis(true, hourOfDay, minute)
        }
    }

    private fun getTimeInMillis(tomorrow: Boolean, hourOfDay: Int, minute: Int): Long {
        val calendar = Calendar.getInstance()

        if (tomorrow) {
            calendar.add(Calendar.DAY_OF_YEAR, 1)
        }

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar.timeInMillis
    }

    fun registAlarmsIfBooted(context: Context, action: String) {
        medicationRealmInfo = OoRealmManager.getMedicineList()
        medicationRealmInfo?.second?.let {
            for (info in it) {
                registAlarm(context, info.alarmId, action, info)
                Log.i(TAG, "$info 재부팅 재등록 완료")
            }
        }
    }

    fun cancelAlarm(ctx: Context, alarmID: Int, action: String) {
        Log.d(TAG, "cancelAlarm : $alarmID")
        val intent = createIntent(ctx, alarmID, action)
        val i = PendingIntent.getBroadcast(ctx, alarmID, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        if (i != null) {
            am?.cancel(i)
            i.cancel()
        }
    }

    fun cancelAllAlarms(ctx: Context, action: String) {
        medicationRealmInfo?.second?.let {
            for (info in it) {
                cancelAlarm(ctx, info.alarmId, action)
                Log.i(TAG, "$info 알람 삭제")
            }
        }
    }

    fun isExistAlarm(ctx: Context, alarmID: Int, action: String): Boolean {
        Log.d(TAG, "isExistAlarm : $alarmID")
        val intent = createIntent(ctx, alarmID, action)
        val sender = PendingIntent.getBroadcast(ctx, alarmID, intent, PendingIntent.FLAG_NO_CREATE)
        return sender != null
    }

    fun createIntent(ctx: Context, alarmId: Int, action: String): Intent {
        val intent = Intent(ctx, OoAlarmReceiver::class.java)
        intent.action = action
        intent.putExtra(OoAlarmReceiver.KEY_ALARM_ID, alarmId)

        return intent
    }

    fun createPendingIntent(
        ctx: Context,
        alarmID: Int,
        action: String
    ): PendingIntent {
        val intent = createIntent(ctx, alarmID, action)
        return PendingIntent.getBroadcast(ctx, alarmID, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    private fun createPendingIntent_Re(
        ctx: Context,
        alarmID: Int,
        action: String
    ): PendingIntent {
        val intent = createIntent(ctx, alarmID, action)
        return PendingIntent.getBroadcast(ctx, -alarmID, intent, PendingIntent.FLAG_CANCEL_CURRENT)
    }

    fun deleteAlarm(ctx: Context, alarmId: Int, alarmInfo: OoRmMedicine) {
        MainActivity.user?.id?.let {seniorId ->
            alarmInfo.medicineId?.let {medicineId ->
                OoRestManager.deleteMedications(seniorId, medicineId) {error, response ->
                    Log.i(TAG, "error is $error, response is $response")
                    response?.let {
                        OoRealmManager.deleteMedicineWithId(medicineId)
                        if(OoRealmManager.getSimultaneousAlarmsCount(alarmId)==0) {
                            cancelAlarm(ctx, alarmId, OoAlarmReceiver.ACTION_ALARM)
                        }
                    } ?:   ctx.runOnUiThread {
                        longToast(R.string.response_error)
                    }
                }
            }
        }
    }
}