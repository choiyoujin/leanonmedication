package com.opusone.leanon.medication.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import com.opusone.leanon.medication.R
import com.opusone.leanon.medication.alarm.OoAlarmManager
import com.opusone.leanon.oorealmmanager.model.OoRmMedicine
import kotlinx.android.synthetic.main.dialog_delete.*
import kotlinx.android.synthetic.main.dialog_error.*

class DeleteDialog(context: Context, var alarmId: Int, var alarmInfo: OoRmMedicine) : Dialog(context) {
    var onDeleteDialog: OnDeleteConfirm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_delete)
        deleteAlarm_text.text = "${alarmInfo.medicineName}\n복약관리를 삭제할까요?"
        cancel_delete.setOnClickListener{
            dismiss()
        }
        confirm_delete.setOnClickListener {
            onDeleteDialog?.let {
                it()
            }
            dismiss()
        }
    }
}

typealias OnDeleteConfirm = () -> Unit